package com.mikesantos.MsFriends.Commands;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.mikesantos.MsFriends.MsFriends;

public class GeneralCommand implements CommandExecutor{

	private MsFriends instance;
	public GeneralCommand(MsFriends instance) {
		this.instance = instance;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(cmd.getName().equalsIgnoreCase("friends")){
			if(!(sender instanceof Player)){
				sender.sendMessage("�cSomente players!");
				return true;
			}
			Player p = (Player)sender;
			if(args.length == 0){
				for(String s : this.instance.getConfig().getStringList("Mensagens.Informacoes"))
					sender.sendMessage(this.instance.translate(s, false));
				return true;
			}
			if(args[0].equalsIgnoreCase("add")){
				if(args.length != 2){
					sender.sendMessage(this.instance.translate("Mensagens.Add_Amigo",true));
					return true;
				}
				Player alvo = Bukkit.getPlayerExact(args[1]);
				if(alvo == null){
					sender.sendMessage(this.instance.translate("Mensagens.Player_Offline", true).replaceAll("<amigo>", args[1]));
					return true;
				}
				if(!alvo.isOnline()){
					sender.sendMessage(this.instance.translate("Mensagens.Player_Offline", true).replaceAll("<amigo>", alvo.getName()));
					return true;
				}
				if(this.instance.pedidos.get(p.getName()) != null && this.instance.pedidos.get(p.getName()).contains(alvo.getName())){
					sender.sendMessage(this.instance.translate("Mensagens.Ja_Pediu", true).replaceAll("<amigo>", alvo.getName()));
					return true;
				}
				List<String> t;
				if(this.instance.pedidos.get(p.getName()) == null){
					t = new ArrayList<String>();
				}else{
					t = this.instance.pedidos.get(p.getName());
				}
				t.add(alvo.getName());
				this.instance.pedidos.put(p.getName(), t);
				sender.sendMessage(this.instance.translate("Mensagens.Pedido_Enviado", true).replaceAll("<amigo>", alvo.getName()));
				for(String s : this.instance.getConfig().getStringList("Mensagens.Solicitando_Amizade")){
					alvo.sendMessage(this.instance.translate(s, false).replaceAll("<player>", p.getName()));
				}
				return true;
			}else
			if(args[0].equalsIgnoreCase("aceitar")){
				if(args.length != 2){
					sender.sendMessage(this.instance.translate("Mensagens.Aceitar_Amigo",true));
					return true;
				}
				Player alvo = Bukkit.getPlayerExact(args[1]);
				if(alvo == null){
					sender.sendMessage(this.instance.translate("Mensagens.Player_Offline", true).replaceAll("<amigo>", args[1]));
					return true;
				}
				if(!alvo.isOnline()){
					sender.sendMessage(this.instance.translate("Mensagens.Player_Offline", true).replaceAll("<amigo>", alvo.getName()));
					return true;
				}
				if(this.instance.pedidos.get(alvo.getName())== null){
					sender.sendMessage("�cVoce nao possui this.instance.pedidos de amizade de " + args[1]);
					return true;
				}
				if(this.instance.pedidos.get(alvo.getName()) != null && !this.instance.pedidos.get(alvo.getName()).contains(p.getName())){
					sender.sendMessage(this.instance.translate("Mensagens.Nao_Pediu_Amizade", true).replaceAll("<amigo>", alvo.getName()));
					return true;
				}
				List<String> t;
				if(this.instance.pedidos.get(p.getName()) == null){
					t = new ArrayList<String>();
				}else{
					t = this.instance.pedidos.get(p.getName());
				}
				t.remove(p.getName());
				this.instance.pedidos.put(alvo.getName(), t);
				this.instance.storage.get(alvo.getName()).addAmigo(p.getName());
				this.instance.storage.get(p.getName()).addAmigo(alvo.getName());
				alvo.sendMessage(this.instance.translate("Mensagens.Aceitou_Amizade2", true).replaceAll("<amigo>", p.getName()));
				p.sendMessage(this.instance.translate("Mensagens.Aceitou_Amizade", true).replaceAll("<amigo>", alvo.getName()));
				return true;
			}else
			if(args[0].equalsIgnoreCase("recusar")){
				if(args.length != 2){
					sender.sendMessage(this.instance.translate("Mensagens.Recusar_Amigo",true));
					return true;
				}
				Player alvo = Bukkit.getPlayerExact(args[1]);
				if(alvo == null){
					sender.sendMessage(this.instance.translate("Mensagens.Player_Offline", true).replaceAll("<amigo>", args[1]));
					return true;
				}
				if(!alvo.isOnline()){
					sender.sendMessage(this.instance.translate("Mensagens.Player_Offline", true).replaceAll("<amigo>", alvo.getName()));
					return true;
				}
				if(this.instance.pedidos.get(alvo.getName())== null){
					sender.sendMessage("�cVoce nao possui this.instance.pedidos de amizade de " + args[1]);
					return true;
				}
				if(this.instance.pedidos.get(alvo.getName()) != null && !this.instance.pedidos.get(alvo.getName()).contains(p.getName())){
					sender.sendMessage(this.instance.translate("Mensagens.Nao_Pediu_Amizade", true).replaceAll("<amigo>", alvo.getName()));
					return true;
				}
				List<String> t;
				if(this.instance.pedidos.get(p.getName()) == null){
					t = new ArrayList<String>();
				}else{
					t = this.instance.pedidos.get(p.getName());
				}
				t.remove(p.getName());
				this.instance.pedidos.put(alvo.getName(), t);
				alvo.sendMessage(this.instance.translate("Mensagens.Recusou_Amizade2", true).replaceAll("<amigo>", p.getName()));
				p.sendMessage(this.instance.translate("Mensagens.Recusou_Amizade", true).replaceAll("<amigo>", alvo.getName()));
				return true;
			}else
			if(args[0].equalsIgnoreCase("remover")){
				if(args.length != 2){
					sender.sendMessage(this.instance.translate("Mensagens.Remover_Amigo",true));
					return true;
				}
				Player alvo = Bukkit.getPlayerExact(args[1]);
				if(alvo == null){
					sender.sendMessage(this.instance.translate("Mensagens.Player_Offline", true).replaceAll("<amigo>", args[1]));
					return true;
				}
				if(!alvo.isOnline()){
					sender.sendMessage(this.instance.translate("Mensagens.Player_Offline", true).replaceAll("<amigo>", alvo.getName()));
					return true;
				}
				if(!this.instance.storage.get(p.getName()).getFriends().contains(alvo.getName())){
					sender.sendMessage(this.instance.translate("Mensagens.Nao_E_Amigo", true).replaceAll("<amigo>", alvo.getName()));
					return true;
				}
				this.instance.storage.get(p.getName()).rmvAmigo(alvo.getName());
				this.instance.storage.get(alvo.getName()).rmvAmigo(p.getName());
				alvo.sendMessage(this.instance.translate("Mensagens.Removeu_Amigo", true).replaceAll("<amigo>", p.getName()));
				p.sendMessage(this.instance.translate("Mensagens.Removeu_Amigo2", true).replaceAll("<amigo>", alvo.getName()));
				return true;
			}else
			if(args[0].equalsIgnoreCase("lista")){
				if(this.instance.storage.get(p.getName()).getFriends().size() == 0){
					sender.sendMessage(this.instance.translate("Mensagens.Voce_nao_tem_amigos", true));
					return true;
				}
				String lista = "";
				List<String> amigos = this.instance.storage.get(p.getName()).getFriends();
				for(String i : amigos){
					String stats = "�f[�cOFF�f]";
					Player a = Bukkit.getPlayerExact(i);
					stats = (a != null && a.isOnline()) ? "�f[�aON�f]" : "�f[�cOFF�f]";
					lista = lista + "�3"+ i + " " + stats + ", ";
				}
				lista = lista.substring(0, lista.length() - 2) + ".";
				sender.sendMessage(this.instance.translate("Mensagens.Lista_Amigos", true).replaceAll("<amigos>", lista));
				return true;
			}
			sender.sendMessage(this.instance.translate("Mensagens.Comando_Invalido", true));
			return true;
		}
		return false;
	}
}
