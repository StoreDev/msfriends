package com.mikesantos.MsFriends;

import java.io.File;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class Friends {

	private MsFriends plugin;
	private String name;
	private List<String> friends;
	public Friends(MsFriends plugin, String name) {
		this.plugin = plugin;
		this.name = name;
		this.friends = new ArrayList<String>();
	}
	public Friends(String name, List<String> list) {
		this.name = name;
		this.friends = list;
	}
	
	public void setup(){
		File f = new File(this.plugin.getDataFolder() + File.separator + "Amigos", this.name + ".yml");
		if(!f.exists())
			try {
				f.createNewFile();
			} catch (IOException e) {
			}
		FileConfiguration fc = YamlConfiguration.loadConfiguration(f);
		if(fc.isSet("Amigos.Lista") && fc.getStringList("Amigos.Lista") != null){
			try {
				this.friends = fc.getStringList("Amigos.Lista"); 
			} catch (NullPointerException e2) {
			}
		}
	}
	
	public void save(){
		File f = new File(this.plugin.getDataFolder() + File.separator + "Amigos", this.name + ".yml");
		FileConfiguration fc = YamlConfiguration.loadConfiguration(f);
		fc.set("Amigos.Lista", this.friends);
		try {
			fc.save(f);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public String getName(){
		return this.name;
	}
	public List<String> getFriends(){
		return this.friends;
	}
	public void addAmigo(String name){
		if(!this.friends.contains(name))
			this.friends.add(name);
	}
	public void rmvAmigo(String name){
		if(this.friends.contains(name))
			this.friends.remove(name);
	}
	public void setAmigos(List<String> list){
		this.friends = list;
	}
	
}
