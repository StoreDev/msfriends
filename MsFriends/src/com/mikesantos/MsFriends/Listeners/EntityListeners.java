package com.mikesantos.MsFriends.Listeners;

import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import com.mikesantos.MsFriends.Friends;
import com.mikesantos.MsFriends.MsFriends;

public class EntityListeners implements Listener{

	private MsFriends instance;
	public EntityListeners(MsFriends instance) {
		this.instance = instance;
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void onEntityDamage(EntityDamageByEntityEvent e) {
		Player attacker = null;
		Player victim = null;
		EntityDamageByEntityEvent sub = (EntityDamageByEntityEvent) e;
		if ((sub.getEntity() instanceof Player) && (sub.getDamager() instanceof Player)) {
			attacker = (Player) sub.getDamager();
			victim = (Player) sub.getEntity();
		}
		if (((sub.getEntity() instanceof Player)) && ((sub.getDamager() instanceof Arrow))) {
			Arrow arrow = (Arrow) sub.getDamager();
			if ((arrow.getShooter() instanceof Player)) {
				attacker = (Player) arrow.getShooter();
				victim = (Player) sub.getEntity();
			}
		}
		if (attacker != null && victim != null) {
			if(this.instance.storage.get(attacker.getName()) == null){
				return;
			}
			Friends f = this.instance.storage.get(attacker.getName());
			if(f.getFriends().contains(victim.getName())){
				e.setCancelled(true);
			}
		}
	}
}
