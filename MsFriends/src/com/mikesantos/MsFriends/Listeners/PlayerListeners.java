package com.mikesantos.MsFriends.Listeners;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import com.mikesantos.MsFriends.Friends;
import com.mikesantos.MsFriends.MsFriends;

public class PlayerListeners implements Listener{

	private MsFriends instance;
	public PlayerListeners(MsFriends instance) {
		this.instance = instance;
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e){
		final Player p = e.getPlayer();
		final MsFriends pl = this.instance;
		Thread th = new Thread(new Runnable() {
			@Override
			public void run() {
				Friends amg = new Friends(pl, p.getName());
				amg.setup();
				pl.storage.put(p.getName(), amg);
				for(Player pp : Bukkit.getServer().getOnlinePlayers()){
					if(amg.getFriends().contains(pp.getName())){
						pp.sendMessage(pl.translate("Mensagens.Amigo_Entrou", true).replaceAll("<amigo>", p.getName()));
					}
				}
			}
		});
		th.start();
	}
	
	@EventHandler
	public void onLeave(PlayerQuitEvent e){
		final Player p = e.getPlayer();
		final MsFriends pl = this.instance;
		Thread th = new Thread(new Runnable() {
			@Override
			public void run() {
				if(pl.storage.containsKey(p.getName())){
					for(Player pp : Bukkit.getServer().getOnlinePlayers()){
						if(pl.storage.get(p.getName()).getFriends().contains(pp.getName())){
							pp.sendMessage(pl.translate("Mensagens.Amigo_Saiu", true).replaceAll("<amigo>", p.getName()));
						}
					}
					pl.storage.get(p.getName()).save();
					pl.storage.remove(p.getName());
				}
			}
		});
		th.start();
	}
	
	@EventHandler
	public void onChat(PlayerCommandPreprocessEvent e){
		String[] arg = e.getMessage().split(" ");
		if(arg[0].equalsIgnoreCase(this.instance.getConfig().getString("Configuracoes.Chat.Comando"))){
			if(!this.instance.storage.containsKey(e.getPlayer().getName())){
				return;
			}
			String msg = "";
			for(int i = 1; i < arg.length; i++){
				msg = msg + arg[i] + " ";
			}
			msg = msg.substring(0, msg.length() - 1) + ".";
			Friends f = this.instance.storage.get(e.getPlayer().getName());
			String tosend = this.instance.translate("Configuracoes.Chat.Formato", true).replaceAll("(?i)<mensagem>", msg);
			e.getPlayer().sendMessage(tosend);
			for(String s : f.getFriends()){
				Player p = Bukkit.getPlayer(s);
				if(p != null && p.isOnline()){
					p.sendMessage(tosend);
				}
			}
			e.setCancelled(true);
		}
	}
}
