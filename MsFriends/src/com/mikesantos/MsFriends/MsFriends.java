package com.mikesantos.MsFriends;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import com.mikesantos.MsFriends.Friends;
import com.mikesantos.MsFriends.Commands.GeneralCommand;
import com.mikesantos.MsFriends.Listeners.EntityListeners;
import com.mikesantos.MsFriends.Listeners.PlayerListeners;

public class MsFriends extends JavaPlugin{

	public HashMap<String, Friends> storage = new LinkedHashMap<String, Friends>();
	public HashMap<String, List<String>> pedidos = new LinkedHashMap<String, List<String>>();
	
	public void onEnable() {
		console("Ligando Plugin...");
		if(!getDescription().getName().equals("MsFriends")){
			getPluginLoader().disablePlugin(this);
			console("&cErro ao Ligar Plugin!");
			console("&cMotivo: &fNome do plugin foi modificado.");
			return;
		}
		saveResource("Amigos/kaway.yml", false);
		console("Amigos/kaway.yml Salvo com sucesso...");
		saveDefaultConfig();
		console("Configuracao padrao Salvo com sucesso...");
		getServer().getPluginManager().registerEvents(new PlayerListeners(this), this);
		getServer().getPluginManager().registerEvents(new EntityListeners(this), this);
		console("Eventos Registrados com sucesso...");
		getCommand("friends").setExecutor(new GeneralCommand(this));
		console("Plugin Ligado com Sucesso...");
		storage.clear();
		if(getServer().getOnlinePlayers().length > 0){
			for(Player p : getServer().getOnlinePlayers()){
				Friends amg = new Friends(this, p.getName());
				amg.setup();
				storage.put(p.getName(), amg);
			}
		}
		super.onEnable();
	}

	public void onDisable() {
		for(Player p : getServer().getOnlinePlayers())
			if(storage.containsKey(p.getName())){
				storage.get(p.getName()).save();
				storage.remove(p.getName());
			}
		super.onDisable();
	}
	
	public String Prefix(){
		return ChatColor.translateAlternateColorCodes('&', getConfig().getString("Prefix","§c[MsAmigos]"));
	}
	
	public String translate(String s, boolean getString){
		if(getString){
			return ChatColor.translateAlternateColorCodes('&', getConfig().getString(s,"§cErro ao pegar Mensagem.")).replaceAll("<prefix>", Prefix());
		}else{
			return ChatColor.translateAlternateColorCodes('&', s).replaceAll("<prefix>", Prefix());
		}
	}
	
	public void console(String s){
		getServer().getConsoleSender().sendMessage(("&3[&bMsFriends&3] &a" + s).replaceAll("&", "§"));
	}
}
